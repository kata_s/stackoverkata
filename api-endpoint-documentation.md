  ---
## Swagger API Documentation
---

This documentation is designed to use the stackoverkata api.

Endpoints are presented in tabular form. In the future, 
supplement with the necessary methods.

For information purposes, the API-Sensors endpoints are shown:

| Method | URL | Description |
 | ------ | ------ | ------ |
| GET | api/user/question/{questionId}/answer | Returns a list of answers by question id |


