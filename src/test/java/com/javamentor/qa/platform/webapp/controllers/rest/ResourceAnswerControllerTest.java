package com.javamentor.qa.platform.webapp.controllers.rest;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.javamentor.qa.platform.BaseTest;
import com.javamentor.qa.platform.models.dto.CommentAnswerDto;
import com.javamentor.qa.platform.models.entity.question.answer.VoteAnswer;
import com.javamentor.qa.platform.models.entity.question.answer.VoteType;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.models.entity.question.answer.Answer;
import com.javamentor.qa.platform.models.entity.user.reputation.Reputation;
import com.jayway.jsonpath.JsonPath;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ResourceAnswerControllerTest extends BaseTest {

    private static final String EMAIL = "test@mail.ru";
    private static final String PASSWORD = "test";
    private static final String QUESTION_ID = "101";
    private static final String QUESTION_ID_WITHOUT_ANSWER = "102";
    private static final String QUESTION_NOT_FOND_ID = "1001";
    private static final String COMMENT_TEXT = "A comment text.";
    private static final Long ANSWER_ID = 101L;
    private static final Long USER_ID = 100L;
    private static final  String NON_EXISTENT_ANSWER_ID = "999";

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private PlatformTransactionManager transactionManager;

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void getAllAnswer() throws Exception {

        mockMvc.perform(get("/api/user/question/" + QUESTION_ID + "/answer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[0].id", is(equalTo(101))))
                .andExpect(jsonPath("$.[0].userId", is(equalTo(100))))
                .andExpect(jsonPath("$.[0].questionId", is(equalTo(101))))
                .andExpect(jsonPath("$.[0].body", is(equalTo("This is a 1-test answer"))))
                .andExpect(jsonPath("$.[0].persisDate", is(equalTo("2023-01-01T00:00:00"))))
                .andExpect(jsonPath("$.[0].isHelpful", is(equalTo(true))))
                .andExpect(jsonPath("$.[0].dateAccept", is(equalTo("2023-12-01T00:00:00"))))

                .andExpect(jsonPath("$.[1].id", is(equalTo(102))))
                .andExpect(jsonPath("$.[1].userId", is(equalTo(100))))
                .andExpect(jsonPath("$.[1].questionId", is(equalTo(101))))
                .andExpect(jsonPath("$.[1].body", is(equalTo("This is a 2-test answer"))))
                .andExpect(jsonPath("$.[1].persisDate", is(equalTo("2023-11-01T00:00:00"))))
                .andExpect(jsonPath("$.[1].isHelpful", is(equalTo(false))))
                .andExpect(jsonPath("$.[1].dateAccept", is(equalTo("2023-11-01T00:00:00"))));
    }

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void getEmptyAnswer() throws Exception {

        mockMvc.perform(get("/api/user/question/" + QUESTION_ID_WITHOUT_ANSWER + "/answer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().string("[]"));
    }

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void getWithoutQuestion() throws Exception {

        mockMvc.perform(get("/api/user/question/" + QUESTION_NOT_FOND_ID + "/answer")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());

    }

    @Test
    @DataSet(value = {
            "datasets/question_controller/question.yml",
            "datasets/user_controller/user.yml",
            "datasets/answer_controller/answer.yml",
            "datasets/answer_controller/commentAnswer.yml",
            "datasets/role_controller/role.yml"
    },
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void saveCommentAnswer_returnCommentAnswerDto() throws Exception {
        // Expected DTO
        CommentAnswerDto expectedDto = createExpectedCommentAnswerDto();

        // Perform request
        mockMvc.perform(post("/api/user/question/{questionId}/answer/{answerId}/comment",
                        QUESTION_ID, ANSWER_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", getToken(EMAIL, PASSWORD))
                        .content(COMMENT_TEXT))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.answerId").value(expectedDto.getAnswerId()))
                .andExpect(jsonPath("$.text").value(expectedDto.getText()))
                .andExpect(jsonPath("$.userId").value(expectedDto.getUserId()))
                .andExpect(jsonPath("$.reputation").value(expectedDto.getReputation()));
    }

    private CommentAnswerDto createExpectedCommentAnswerDto() {
        CommentAnswerDto expectedDto = new CommentAnswerDto();

        expectedDto.setAnswerId(ANSWER_ID);
        expectedDto.setLastRedactionDate(LocalDateTime.of(
                LocalDate.of(2024, 1, 1),
                LocalTime.of(0, 0, 0)));
        expectedDto.setPersistDate(LocalDateTime.of(
                LocalDate.of(2023, 1, 1),
                LocalTime.of(0, 0, 0)));
        expectedDto.setText(COMMENT_TEXT);
        expectedDto.setUserId(USER_ID);
        expectedDto.setImageLink("http://images.test/image.jpg");
        expectedDto.setReputation(5L);
        return expectedDto;
    }

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void markAnswerToDelete_ShouldMarkAnswerAsDeleted_WhenAnswerExists() throws Exception {

        mockMvc.perform(delete("/api/user/question/" + QUESTION_ID + "/answer/" + ANSWER_ID)
                        .header("Authorization", getToken(EMAIL, PASSWORD)))
                .andExpect(status().isOk())
                .andDo(print());

        new TransactionTemplate(transactionManager).execute(status -> {
            entityManager.flush();

            Answer answer = entityManager.find(Answer.class, ANSWER_ID);
            assertNotNull(answer);
            assertTrue(answer.getIsDeleted());

            status.setRollbackOnly();

            return null;
        });
    }

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void markAnswerToDelete_ShouldReturnNotFound_WhenAnswerDoesNotExist() throws Exception {

        mockMvc.perform(delete("/api/user/question/" + QUESTION_NOT_FOND_ID + "/answer/" + QUESTION_ID_WITHOUT_ANSWER)
                        .header("Authorization", getToken(EMAIL, PASSWORD)))
                .andExpect(status().isNotFound())
                .andExpect(content().string(is(emptyOrNullString())));

    }

    // Тест когда передается нечисловой ID
    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void markAnswerToDelete_ShouldReturnBadRequest_WhenIdIsNonNumeric() throws Exception {
        mockMvc.perform(delete("/api/user/question/invalid/answer/invalid")
                        .header("Authorization", getToken(EMAIL, PASSWORD)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value(1));;
    }


    // Тест для аутентифицированного пользователя без прав
    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void markAnswerToDelete_ShouldFail_WhenUserNotAuthorized() throws Exception {
        String anotherUserToken = obtainTokenOfUserWithoutDeleteRights();
        mockMvc.perform(delete("/api/user/question/" + QUESTION_ID + "/answer/" + ANSWER_ID)
                        .header("Authorization", anotherUserToken))
                .andExpect(status().isForbidden());
    }

    public String obtainTokenOfUserWithoutDeleteRights() throws Exception {

        String body = "{\"login\":\"" + EMAIL + "\", \"pass\":\"" + PASSWORD + "\"}";


        MvcResult result = mockMvc.perform(post("/api/auth/token")
                        .content(body)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        String responseContent = result.getResponse().getContentAsString();

        return JsonPath.parse(responseContent).read("$.token");
    }
    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/users.yml",
            "datasets/question_controller/questions.yml",
            "datasets/answer_controller/answers.yml",
            "datasets/reputation_controller/reputations.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void upVoteTest() throws Exception {

        User userPreVote = entityManager.find(User.class, USER_ID);
        Reputation reputationPreVote = entityManager.find(Reputation.class,USER_ID);
        int reputationPre = reputationPreVote.getCount();
        Answer answerPreVote = entityManager.find(Answer.class, ANSWER_ID);


        mockMvc.perform(post("/api/user/question/{questionId}/answer/{answerId}/upVote", QUESTION_ID, ANSWER_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk());

        Reputation reputationPostVote= entityManager.find(Reputation.class,USER_ID);
        int reputationPost = reputationPostVote.getCount();
        Answer answerPostVote = entityManager.find(Answer.class, ANSWER_ID);


//     Проверяем, что ответ не изменился
    assertEquals(answerPreVote.getId(), answerPostVote.getId());


    // Проверяем, что репутация увеличилась на 10
        assertEquals(reputationPre, reputationPost, reputationPre + 10);

    }
    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void upVoteAnswer_WhenVotingForOwnAnswer() throws Exception {

        mockMvc.perform(post("/api/user/question/{questionId}/answer/{answerId}/upVote", QUESTION_ID, ANSWER_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andExpect(status().isConflict());

    }

    @Test
    @DataSet(value = {
            "datasets/role_controller/role.yml",
            "datasets/user_controller/user.yml",
            "datasets/question_controller/question.yml",
            "datasets/answer_controller/answer.yml"},
            disableConstraints = true)
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void upVoteAnswer_VotingForNonExistentAnswer() throws Exception {

        mockMvc.perform(post("/api/user/question/{questionId}/answer/{answerId}/upVote", QUESTION_ID, NON_EXISTENT_ANSWER_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andExpect(status().isNotFound());
    }


}


