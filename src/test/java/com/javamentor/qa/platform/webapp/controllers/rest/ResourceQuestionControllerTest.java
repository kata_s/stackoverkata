package com.javamentor.qa.platform.webapp.controllers.rest;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.DataSetFormat;
import com.github.database.rider.core.api.exporter.ExportDataSet;
import com.javamentor.qa.platform.BaseTest;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ResourceQuestionControllerTest extends BaseTest {

    @Test
    @DataSet(value = {
            "dataset/question_controller/question.yml",
            "dataset/user_controller/user.yml",
            "dataset/role_controller/role.yml"})
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void getQuestionByIdTest() throws Exception {
        String QUESTION_ID = "101";
        String EMAIL = "test@mail.ru";
        String PASSWORD = "test";
        mockMvc.perform(get("/api/user/question/" + QUESTION_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(equalTo(101))))
                .andExpect(jsonPath("$[0].title", is(equalTo("1-test-question"))))
                .andExpect(jsonPath("$[0].description", is(equalTo("Test question"))))
                .andExpect(jsonPath("$[0].persistDate", is(equalTo("2023-12-01"))))
                .andExpect(jsonPath("$[0].userId", is(equalTo(100))))
                .andExpect(jsonPath("$[0].lastRedactionDate", is(equalTo("2023-12-10"))));

    }

    @Test
    @DataSet(value = {
            "dataset/question_controller/question.yml",
            "dataset/user_controller/user.yml",
            "dataset/role_controller/role.yml"})
    @ExportDataSet(format = DataSetFormat.YML, outputName = "target/exported/yml/allTables.yml")
    public void Question_Id_Not_Found_Test() throws Exception {
        String EMAIL = "test@mail.ru";
        String PASSWORD = "test";
        String QUESTION_NOT_FOND_ID = "999";

        mockMvc.perform(get("/api/user/question/" + QUESTION_NOT_FOND_ID)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization", "" + getToken(EMAIL, PASSWORD)))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$").doesNotExist());
    }


}
