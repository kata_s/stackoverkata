package com.javamentor.qa.platform.service.abstracts.dto;

import com.javamentor.qa.platform.models.dto.QuestionCommentDto;


public interface QuestionCommentDtoService {
    QuestionCommentDto QuestionCommentDto(Long useId, Long questionID, String text);


}
