package com.javamentor.qa.platform.service.abstracts.model;

import com.javamentor.qa.platform.models.dto.QuestionCreateDto;
import com.javamentor.qa.platform.models.entity.question.Question;
import com.javamentor.qa.platform.models.entity.question.Tag;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.repository.ReadWriteService;

import java.util.List;

public interface QuestionService extends ReadWriteService<Question, Long> {

    Question addQuestion(QuestionCreateDto questionCreateDto, List<Tag> tags, User user);

    Long getCountQuestion ();

}
