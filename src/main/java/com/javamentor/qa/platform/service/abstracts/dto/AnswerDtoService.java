package com.javamentor.qa.platform.service.abstracts.dto;

import com.javamentor.qa.platform.models.dto.AnswerDto;

import java.util.List;

public interface AnswerDtoService {
    List<AnswerDto> getAllAnswersDto(Long questionId, Long userId);

    void updateAnswerBody(Long userId, String body, Long answerId, Long questionId);
}
