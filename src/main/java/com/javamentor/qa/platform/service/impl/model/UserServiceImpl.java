package com.javamentor.qa.platform.service.impl.model;

import com.javamentor.qa.platform.converters.UserConverter;
import com.javamentor.qa.platform.dao.abstracts.model.UserDao;
import com.javamentor.qa.platform.models.dto.UserRegistrationDto;
import com.javamentor.qa.platform.models.entity.user.Role;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.model.UserService;
import com.javamentor.qa.platform.service.impl.repository.ReadWriteServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImpl extends ReadWriteServiceImpl<User, Long> implements UserService {

    private final UserDao userDao;
    private final EmailService emailService;
    private final UserConverter userConverter;
    private final Role role = new Role("USER");

    public UserServiceImpl(UserDao userDao, EmailService emailService, UserConverter userConverter) {
        super(userDao);
        this.userDao = userDao;
        this.emailService = emailService;
        this.userConverter = userConverter;
    }

    @Transactional
    @Override
    public Optional<User> getByEmail(String email) {
        return userDao.getByEmail(email);
    }

    @Override
    @Transactional
    public void registration(UserRegistrationDto userRegistrationDto) {
        User user = (userConverter.userRegistrationDtoToUser(userRegistrationDto));
        user.setIsEnabled(false);
        user.setPersistDateTime(LocalDateTime.now());
        user.setRole(role);
        super.persist(user);
        Integer code = user.getEmail().hashCode();
        String message = String.format("Пройдите по ссылке для завершения регистрации: http://localhost:8080/api/user/registration/verify/%d", code);
        emailService.send(user.getEmail(), "Регистрация", message);
    }

    @Override
    @Transactional
    public boolean verification(User user, Integer code) {
        if (code.equals(user.getEmail().hashCode())) {
            user.setIsEnabled(true);
            super.update(user);
            return true;
        }
        return false;
    }

}
