package com.javamentor.qa.platform.service.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.CommentAnswerDtoDao;
import com.javamentor.qa.platform.exception.AnswerException;
import com.javamentor.qa.platform.models.dto.CommentAnswerDto;
import com.javamentor.qa.platform.models.entity.question.answer.Answer;
import com.javamentor.qa.platform.models.entity.question.answer.CommentAnswer;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.dto.CommentAnswerDtoService;
import com.javamentor.qa.platform.service.abstracts.model.AnswerService;
import com.javamentor.qa.platform.service.abstracts.model.CommentAnswerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class CommentAnswerDtoServiceImpl implements CommentAnswerDtoService {
    private final CommentAnswerDtoDao commentAnswerDtoDao;
    private final AnswerService answerService;
    private final CommentAnswerService commentAnswerService;

    @Autowired
    public CommentAnswerDtoServiceImpl(CommentAnswerDtoDao commentAnswerDtoDao, AnswerService answerService, CommentAnswerService commentAnswerService) {
        this.commentAnswerDtoDao = commentAnswerDtoDao;
        this.answerService = answerService;
        this.commentAnswerService = commentAnswerService;
    }

    @Override
    public CommentAnswerDto addCommentToAnswer(User user, Long answerId, String commentText) {
        Optional<Answer> optionalAnswer = answerService.getById(answerId);
        optionalAnswer.orElseThrow(() -> new AnswerException(String.format("Ответа с id %d не существует", answerId)));

        return optionalAnswer.map(answer -> {
            CommentAnswer commentAnswer = new CommentAnswer(commentText, user);
            commentAnswer.setAnswer(answer);
            commentAnswerService.persist(commentAnswer);
            return commentAnswerDtoDao.getCommentAnswerDto(commentAnswer, user);
        }).orElse(null);
    }
}
