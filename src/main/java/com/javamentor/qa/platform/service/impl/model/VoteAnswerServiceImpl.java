package com.javamentor.qa.platform.service.impl.model;

import com.javamentor.qa.platform.dao.abstracts.model.VoteAnswerDao;
import com.javamentor.qa.platform.dao.abstracts.repository.ReadWriteDao;
import com.javamentor.qa.platform.exception.VoteException;
import com.javamentor.qa.platform.models.entity.question.answer.Answer;
import com.javamentor.qa.platform.models.entity.question.answer.VoteAnswer;
import com.javamentor.qa.platform.models.entity.question.answer.VoteType;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.models.entity.user.reputation.Reputation;
import com.javamentor.qa.platform.models.entity.user.reputation.ReputationType;
import com.javamentor.qa.platform.service.abstracts.model.AnswerService;
import com.javamentor.qa.platform.service.abstracts.model.ReputationService;
import com.javamentor.qa.platform.service.abstracts.model.UserService;
import com.javamentor.qa.platform.service.abstracts.model.VoteAnswerService;
import com.javamentor.qa.platform.service.impl.repository.ReadWriteServiceImpl;
import com.javamentor.qa.platform.webapp.controllers.rest.ResourceAnswerController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class VoteAnswerServiceImpl extends ReadWriteServiceImpl<VoteAnswer, Long> implements VoteAnswerService {
    private static final Logger logger = LoggerFactory.getLogger(ResourceAnswerController.class);

    private final AnswerService answerService;
    private final UserService userService;
    private final ReputationService reputationService;
    private final VoteAnswerDao voteAnswerDao;

    public VoteAnswerServiceImpl(ReadWriteDao<VoteAnswer, Long> readWriteDao, AnswerService answerService, UserService userService, ReputationService reputationService, VoteAnswerDao voteAnswerDao) {
        super(readWriteDao);
        this.answerService = answerService;
        this.userService = userService;
        this.reputationService = reputationService;
        this.voteAnswerDao = voteAnswerDao;
    }

    @Override
    public Optional<VoteAnswer> getByAnswerIdAndUserId(Long answerId, Long userId) {
        return voteAnswerDao.getByAnswerIdAndUserId(answerId, userId);
    }

    @Transactional
    @Override
    public Integer downVoteAnswer(Long answerId, Long userId) {
        //Если текущий пользователь является автором ответа.
        if (answerService.getById(answerId).get().getUser().equals(userService.getById(userId).get())) {
            throw new VoteException("Пользователь не может голосовать за свой вопрос");
        }
        //Проголосовал ли уже пользователь за данный ответ.
        if (getByAnswerIdAndUserId(answerId, userService.getById(userId).get().getId()).isPresent()) {
            throw new VoteException("Пользователь проголосовал 'за' ранее");
        }
        //Если не голосовал, создаем голос
        VoteAnswer voteAnswer = new VoteAnswer();
        voteAnswer.setVoteType(VoteType.DOWN);
        voteAnswer.setAnswer(answerService.getById(answerId).get());
        voteAnswer.setUser(userService.getById(userId).get());
        voteAnswer.setPersistDateTime(LocalDateTime.now());
        voteAnswerDao.persist(voteAnswer);
        logger.info("Down vote for answer with ID {} is persisted", answerId);
        User author = answerService.getById(answerId).get().getUser();
        //Есть ли у автора репутация?
        Reputation reputation;
        if (reputationService.getByAnswerIdUserId(answerId, userId).isEmpty()) {
            reputation = new Reputation();
            reputation.setAnswer(answerService.getById(answerId).get());
            reputation.setType(ReputationType.Answer);
            reputation.setCount(-5);
            reputation.setPersistDate(LocalDateTime.now());
            reputation.setAuthor(author);
            reputation.setSender(author);
            reputationService.persist(reputation);
        } else {
            reputation = reputationService.getByAnswerIdUserId(answerId, userId).get();
            reputation.setCount(reputation.getCount() - 5);
            reputationService.update(reputation);
            logger.info("Count for answer with ID {} is {}", answerId, reputation.getCount());
        }
        logger.info("Reputation with ID {} is updated", reputation.getId());
        return answerService.getById(answerId).get().getVoteAnswers().size();
    }
    @Transactional
    @Override
    public void upVoteAnswer(Answer answer, User user) {
        VoteAnswer voteAnswer = new VoteAnswer();
        voteAnswer.setVoteType(VoteType.UP);
        voteAnswer.setAnswer(answer);
        voteAnswer.setUser(user);
        voteAnswer.setPersistDateTime(LocalDateTime.now());
        voteAnswerDao.persist(voteAnswer);

        if (reputationService.getByAnswerIdUserId(answer.getId(), user.getId()).isEmpty()) {
            Reputation newReputation = new Reputation();
            newReputation.setAnswer(answer);
            newReputation.setType(ReputationType.Answer);
            newReputation.setCount(10);
            newReputation.setPersistDate(LocalDateTime.now());
            newReputation.setAuthor(answer.getUser());
            newReputation.setSender(user);
            reputationService.persist(newReputation);
        } else {
            Reputation currentRep = reputationService.getByAnswerIdUserId(answer.getId(), user.getId()).get();
            currentRep.setCount(currentRep.getCount() + 10);
            reputationService.update(currentRep);
        }
        logger.info("Answer with ID {} upvoted successfully. Current vote count: {}", answer.getId(), answer.getVoteAnswers().size());
    }
}