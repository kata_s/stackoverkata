package com.javamentor.qa.platform.mapper;

import com.javamentor.qa.platform.models.dto.QuestionDto;
import com.javamentor.qa.platform.models.entity.question.Question;
import io.swagger.v3.oas.annotations.media.Schema;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
@Schema(description = "Converter QuestionCreateDto -> Question <-> QuestionDto")
public interface QuestionMapper {

    @Mappings(value = {
            @Mapping(target = "authorId", source = "user.id"),
            @Mapping(target = "authorName", source = "user.username"),
            @Mapping(target = "listTagDto", source = "tags")})
    QuestionDto questionToQuestionDto(Question question);

    @InheritInverseConfiguration
    QuestionDto entityToQuestionDto(Question question);
}
