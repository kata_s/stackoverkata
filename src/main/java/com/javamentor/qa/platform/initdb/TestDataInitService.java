package com.javamentor.qa.platform.initdb;

import com.javamentor.qa.platform.models.entity.question.CommentQuestion;
import com.javamentor.qa.platform.models.entity.question.Question;
import com.javamentor.qa.platform.models.entity.question.Tag;
import com.javamentor.qa.platform.models.entity.question.VoteQuestion;
import com.javamentor.qa.platform.models.entity.question.answer.Answer;
import com.javamentor.qa.platform.models.entity.question.answer.CommentAnswer;
import com.javamentor.qa.platform.models.entity.user.Role;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.models.entity.user.UserFavoriteQuestion;
import com.javamentor.qa.platform.models.entity.user.reputation.Reputation;
import com.javamentor.qa.platform.models.entity.user.reputation.ReputationType;
import com.javamentor.qa.platform.service.abstracts.model.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class TestDataInitService {
    private final RoleService roleService;
    private final UserService userService;
    private final TagService tagService;
    private final QuestionService questionService;
    private final AnswerService answerService;
    private final ReputationService reputationService;

    public TestDataInitService(RoleService roleService, UserService userService, TagService tagService, QuestionService questionService, AnswerService answerService, ReputationService reputationService) {
        this.roleService = roleService;
        this.userService = userService;
        this.tagService = tagService;
        this.questionService = questionService;
        this.answerService = answerService;
        this.reputationService = reputationService;
    }

    public void persistEntity() {
        createRoles();
        createUsers();
        createTags();
        createQuestions();
        createAnswers();
        createReputations();
    }
    @Transactional
    public void createEntity() {
        persistEntity();
        roleService.updateAll(roles);
        userService.updateAll(users);
        tagService.updateAll(tags);
        questionService.updateAll(questions);
        answerService.updateAll(answers);
        reputationService.updateAll(reputations);
    }

    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    List<Role> roles = new ArrayList<>();

    private void createRoles() {
        Role admin = new Role();
        admin.setName("ROLE_ADMIN");
        Role user = new Role();
        user.setName("ROLE_USER");
        Role moderator = new Role();
        moderator.setName("ROLE_MODERATOR");
        roles.add(admin);
        roles.add(user);
        roles.add(moderator);
        roleService.persistAll(roles);
    }
    List<User> users = new ArrayList<>();

    private void createUsers() {
        User johndoe = new User();
        johndoe.setEmail("john@example.com");
        johndoe.setPassword(passwordEncoder().encode("password123"));
        johndoe.setFullName("John Doe");
        johndoe.setPersistDateTime(LocalDateTime.now());
        johndoe.setIsEnabled(true);
        johndoe.setIsDeleted(false);
        johndoe.setCity("New York");
        johndoe.setLinkSite("example.com");
        johndoe.setLinkGitHub("github.com/johndoe");
        johndoe.setLinkVk("vk.com/johndoe");
        johndoe.setAbout("About John Doe");
        johndoe.setImageLink("image_link.png");
        johndoe.setLastUpdateDateTime(LocalDateTime.now());
        johndoe.setNickname("johndoe");
        johndoe.setRole(roleService.getByName("ROLE_ADMIN").get());
        User maxim = new User();
        maxim.setPassword(passwordEncoder().encode("password123"));
        maxim.setFullName("Maxim");
        maxim.setPersistDateTime(LocalDateTime.now());
        maxim.setIsEnabled(true);
        maxim.setIsDeleted(false);
        maxim.setCity("Irkutsk");
        maxim.setLinkSite("example.com");
        maxim.setLinkGitHub("github.com/maxim");
        maxim.setLinkVk("vk.com/maxim");
        maxim.setAbout("About Maxim");
        maxim.setImageLink("image_link.png");
        maxim.setLastUpdateDateTime(LocalDateTime.now());
        maxim.setNickname("maxim");
        maxim.setRole(roleService.getByName("ROLE_USER").get());
        User denis = new User();
        denis.setEmail("denis@example.com");
        denis.setPassword(passwordEncoder().encode("password123"));
        denis.setFullName("Denis");
        denis.setPersistDateTime(LocalDateTime.now());
        denis.setIsEnabled(true);
        denis.setIsDeleted(false);
        denis.setCity("Ryazan");
        denis.setLinkSite("example.com");
        denis.setLinkGitHub("github.com/denis");
        denis.setLinkVk("vk.com/denis");
        denis.setAbout("About Denis");
        denis.setImageLink("image_link.png");
        denis.setLastUpdateDateTime(LocalDateTime.now());
        denis.setNickname("denis");
        denis.setRole(roleService.getByName("ROLE_USER").get());
        User egor = new User();
        egor.setEmail("egor@example.com");
        egor.setPassword(passwordEncoder().encode("password123"));
        egor.setFullName("Egor");
        egor.setPersistDateTime(LocalDateTime.now());
        egor.setIsEnabled(true);
        egor.setIsDeleted(false);
        egor.setCity("Moscow");
        egor.setLinkSite("example.com");
        egor.setLinkGitHub("github.com/egor");
        egor.setLinkVk("vk.com/egor");
        egor.setAbout("About Egor");
        egor.setImageLink("image_link.png");
        egor.setLastUpdateDateTime(LocalDateTime.now());
        egor.setNickname("egor");
        egor.setRole(roleService.getByName("ROLE_MODERATOR").get());
        User mihail = new User();
        mihail.setEmail("mihail@example.com");
        mihail.setPassword(passwordEncoder().encode("password123"));
        mihail.setFullName("Mihail");
        mihail.setPersistDateTime(LocalDateTime.now());
        mihail.setIsEnabled(true);
        mihail.setIsDeleted(false);
        mihail.setCity("Syzran");
        mihail.setLinkSite("example.com");
        mihail.setLinkGitHub("github.com/mihail");
        mihail.setLinkVk("vk.com/mihail");
        mihail.setAbout("About Mihail");
        mihail.setImageLink("image_link.png");
        mihail.setLastUpdateDateTime(LocalDateTime.now());
        mihail.setNickname("mihail");
        mihail.setRole(roleService.getByName("ROLE_USER").get());
        User vasiliy = new User();
        vasiliy.setEmail("vasiliy@example.com");
        vasiliy.setPassword(passwordEncoder().encode("password123"));
        vasiliy.setFullName("Vasiliy");
        vasiliy.setPersistDateTime(LocalDateTime.now());
        vasiliy.setIsEnabled(true);
        vasiliy.setIsDeleted(false);
        vasiliy.setCity("Berdyansk");
        vasiliy.setLinkSite("example.com");
        vasiliy.setLinkGitHub("github.com/vasiliy");
        vasiliy.setLinkVk("vk.com/vasiliy");
        vasiliy.setAbout("About Vasiliy");
        vasiliy.setImageLink("image_link.png");
        vasiliy.setLastUpdateDateTime(LocalDateTime.now());
        vasiliy.setNickname("vasiliy");
        vasiliy.setRole(roleService.getByName("ROLE_USER").get());
        users.add(johndoe);
        users.add(maxim);
        users.add(denis);
        users.add(egor);
        users.add(mihail);
        users.add(vasiliy);
        userService.persistAll(users);
    }

    List<Tag> tags = new ArrayList<>();

    private void createTags() {
        Tag programming = new Tag();
        programming.setName("programming");
        programming.setDescription("about programming");
        programming.setPersistDateTime(LocalDateTime.now());
        programming.setQuestions(new ArrayList<Question>());
        Tag gaming = new Tag();
        gaming.setName("gaming");
        gaming.setDescription("about gaming");
        gaming.setPersistDateTime(LocalDateTime.now());
        gaming.setQuestions(new ArrayList<Question>());
        Tag teaching = new Tag();
        teaching.setName("teaching");
        teaching.setDescription("about teaching");
        teaching.setPersistDateTime(LocalDateTime.now());
        teaching.setQuestions(new ArrayList<Question>());
        Tag relax = new Tag();
        relax.setName("relax");
        relax.setDescription("about relax");
        relax.setPersistDateTime(LocalDateTime.now());
        relax.setQuestions(new ArrayList<Question>());
        tags.add(programming);
        tags.add(gaming);
        tags.add(teaching);
        tags.add(relax);
        tagService.persistAll(tags);
    }

    List<Question> questions = new ArrayList<>();

    private void createQuestions() {
        Question question1 = new Question();
        question1.setTitle("Spring");
        question1.setDescription("About Spring");
        question1.setPersistDateTime(LocalDateTime.now());
        question1.setUser(users.get(0));
        question1.setTags(tags);
        question1.setLastUpdateDateTime(LocalDateTime.now());
        question1.setIsDeleted(false);
        question1.setAnswers(new ArrayList<Answer>());
        question1.setCommentQuestions(new ArrayList<CommentQuestion>());
        question1.setUserFavoriteQuestions(new ArrayList<UserFavoriteQuestion>());
        question1.setVoteQuestions(new ArrayList<VoteQuestion>());
        Question question2 = new Question();
        question2.setTitle("Hibernate");
        question2.setDescription("About Hibernate");
        question2.setPersistDateTime(LocalDateTime.now());
        question2.setUser(users.get(3));
        question2.setTags(tags);
        question2.setLastUpdateDateTime(LocalDateTime.now());
        question2.setIsDeleted(false);
        question2.setAnswers(new ArrayList<Answer>());
        question2.setCommentQuestions(new ArrayList<CommentQuestion>());
        question2.setUserFavoriteQuestions(new ArrayList<UserFavoriteQuestion>());
        question2.setVoteQuestions(new ArrayList<VoteQuestion>());
        Question question3 = new Question();
        question3.setTitle("Maven");
        question3.setDescription("About Maven");
        question3.setPersistDateTime(LocalDateTime.now());
        question3.setUser(users.get(2));
        question3.setTags(tags);
        question3.setLastUpdateDateTime(LocalDateTime.now());
        question3.setIsDeleted(false);
        question3.setAnswers(new ArrayList<Answer>());
        question3.setCommentQuestions(new ArrayList<CommentQuestion>());
        question3.setUserFavoriteQuestions(new ArrayList<UserFavoriteQuestion>());
        question3.setVoteQuestions(new ArrayList<VoteQuestion>());
        Question question4 = new Question();
        question4.setTitle("Docker");
        question4.setDescription("About Docker");
        question4.setPersistDateTime(LocalDateTime.now());
        question4.setUser(users.get(1));
        question4.setTags(tags);
        question4.setLastUpdateDateTime(LocalDateTime.now());
        question4.setIsDeleted(false);
        question4.setAnswers(new ArrayList<Answer>());
        question4.setCommentQuestions(new ArrayList<CommentQuestion>());
        question4.setUserFavoriteQuestions(new ArrayList<UserFavoriteQuestion>());
        question4.setVoteQuestions(new ArrayList<VoteQuestion>());
        questions.add(question1);
        questions.add(question2);
        questions.add(question3);
        questions.add(question4);
        questionService.persistAll(questions);
    }

    List<Answer> answers = new ArrayList<>();

    private void createAnswers() {
        Answer answer1 = new Answer();
        answer1.setPersistDateTime(LocalDateTime.now());
        answer1.setUpdateDateTime(LocalDateTime.now());
        answer1.setQuestion(questions.get(1));
        answer1.setUser(users.get(2));
        answer1.setHtmlBody("answer1.html");
        answer1.setIsHelpful(false);
        answer1.setIsDeleted(false);
        answer1.setIsDeletedByModerator(false);
        answer1.setDateAcceptTime(LocalDateTime.now());
        answer1.setCommentAnswers(new ArrayList<CommentAnswer>());
        Answer answer2 = new Answer();
        answer2.setPersistDateTime(LocalDateTime.now());
        answer2.setUpdateDateTime(LocalDateTime.now());
        answer2.setQuestion(questions.get(2));
        answer2.setUser(users.get(1));
        answer2.setHtmlBody("answer2.html");
        answer2.setIsHelpful(false);
        answer2.setIsDeleted(false);
        answer2.setIsDeletedByModerator(false);
        answer2.setDateAcceptTime(LocalDateTime.now());
        answer2.setCommentAnswers(new ArrayList<CommentAnswer>());
        Answer answer3 = new Answer();
        answer3.setPersistDateTime(LocalDateTime.now());
        answer3.setUpdateDateTime(LocalDateTime.now());
        answer3.setQuestion(questions.get(0));
        answer3.setUser(users.get(1));
        answer3.setHtmlBody("answer3.html");
        answer3.setIsHelpful(false);
        answer3.setIsDeleted(false);
        answer3.setIsDeletedByModerator(false);
        answer3.setDateAcceptTime(LocalDateTime.now());
        answer3.setCommentAnswers(new ArrayList<CommentAnswer>());
        Answer answer4 = new Answer();
        answer4.setPersistDateTime(LocalDateTime.now());
        answer4.setUpdateDateTime(LocalDateTime.now());
        answer4.setQuestion(questions.get(3));
        answer4.setUser(users.get(0));
        answer4.setHtmlBody("answer4.html");
        answer4.setIsHelpful(false);
        answer4.setIsDeleted(false);
        answer4.setIsDeletedByModerator(false);
        answer4.setDateAcceptTime(LocalDateTime.now());
        answer4.setCommentAnswers(new ArrayList<CommentAnswer>());
        answers.add(answer1);
        answers.add(answer2);
        answers.add(answer3);
        answers.add(answer4);
        answerService.persistAll(answers);
    }

    List<Reputation> reputations = new ArrayList<>();

    private void createReputations() {
        Reputation reputation1 = new Reputation();
        reputation1.setPersistDate(LocalDateTime.now());
        reputation1.setAuthor(users.get(1));
        reputation1.setSender(users.get(1));
        reputation1.setCount(3);
        reputation1.setType(ReputationType.Question);
        reputation1.setQuestion(questionService.getAll().get(1));
        Reputation reputation2 = new Reputation();
        reputation2.setPersistDate(LocalDateTime.now());
        reputation2.setAuthor(users.get(3));
        reputation2.setSender(users.get(3));
        reputation2.setCount(3);
        reputation2.setType(ReputationType.Answer);
        reputation2.setAnswer(answerService.getAll().get(1));
        Reputation reputation3 = new Reputation();
        reputation3.setPersistDate(LocalDateTime.now());
        reputation3.setAuthor(users.get(2));
        reputation3.setSender(users.get(0));
        reputation3.setCount(3);
        reputation3.setType(ReputationType.Question);
        reputation3.setQuestion(questionService.getAll().get(1));
        reputations.add(reputation1);
        reputations.add(reputation2);
        reputations.add(reputation3);
        reputationService.persistAll(reputations);
    }
}
