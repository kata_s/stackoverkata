package com.javamentor.qa.platform.webapp.controllers.rest;

import com.javamentor.qa.platform.exception.VoteException;
import com.javamentor.qa.platform.models.dto.AnswerDto;
import com.javamentor.qa.platform.models.dto.CommentAnswerDto;
import com.javamentor.qa.platform.models.entity.question.answer.Answer;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.dto.AnswerDtoService;
import com.javamentor.qa.platform.service.abstracts.dto.CommentAnswerDtoService;
import com.javamentor.qa.platform.service.abstracts.model.AnswerService;
import com.javamentor.qa.platform.service.abstracts.model.QuestionService;
import com.javamentor.qa.platform.service.abstracts.model.UserService;
import com.javamentor.qa.platform.service.abstracts.model.VoteAnswerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/user/question/{questionId}/answer")
@AllArgsConstructor
@Tag(name = "ResourceAnswerController")
public class ResourceAnswerController {

    private final AnswerService answerService;
    private final AnswerDtoService answerDtoService;
    private final QuestionService questionService;
    private final UserService userService;
    private final VoteAnswerService voteAnswerService;
    private final CommentAnswerDtoService commentAnswerDtoService;

    @Operation(
            summary = "Получение списка ответов",
            description = "Получение списка DTO ответов по уникальному идентификатору (Id) вопроса")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ответы успешно получены"),
            @ApiResponse(responseCode = "404", description = "Вопрос c id  (Id) не был найден")})
    @GetMapping
    public ResponseEntity<List<AnswerDto>> getAllAnswers(
            @PathVariable("questionId") Long questionId,
            @AuthenticationPrincipal UserDetails userDetails) {
        if (!questionService.existsById(questionId)) {
            log.warn("Not found question with id: {}", questionId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        Optional<User> recentUser = userService.getByEmail(userDetails.getUsername());
        log.info("Get answers with user id: {}", recentUser.get().getId());
        log.info("Get answers with question id: {}", questionId);
        return new ResponseEntity<>(answerDtoService.getAllAnswersDto(questionId, recentUser.get().getId()), HttpStatus.OK);
    }

    @Operation(
            summary = "Уменьшить оценку ответа на 5",
            description = "Уменьшение оценки ответа по уникальному идентификатору (Id) ответа")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Голос успешно зачтен, возвращаем количество голосов."),
            @ApiResponse(responseCode = "400", description = "Вы уже голосовали за данный ответ ранее."),
            @ApiResponse(responseCode = "404", description = "Ответ не найден")
    })
    @PostMapping("{id}/downVote")
    public ResponseEntity<Integer> downVoteAnswer(
            @PathVariable Long questionId,
            @PathVariable Long id,
            @AuthenticationPrincipal UserDetails userDetails) {
        Optional<User> recentUser = userService.getByEmail(userDetails.getUsername());
        Optional<Answer> recentAnswerOptional = answerService.getById(id);
        if (recentAnswerOptional.isEmpty()) {
            log.warn("Not found answer with id: {}", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        int count = recentAnswerOptional.get().getVoteAnswers().size();
        voteAnswerService.downVoteAnswer(id, recentUser.get().getId());
        if (count < recentAnswerOptional.get().getVoteAnswers().size()) {
            //Голос успешно засчитан
            return new ResponseEntity<>(recentAnswerOptional.get().getVoteAnswers().size(), HttpStatus.OK);
        } else {
            //Вы уже голосовали за этот ответ ранее
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @Operation(summary = "Обновление тела комментария по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ответ успешно обновлён"),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе"),
            @ApiResponse(responseCode = "404", description = "Вопрос c id (Id) не был найден")})
    @PutMapping("/{answerId}/body")
    public ResponseEntity<AnswerDto> updateAnswerBody(
            @PathVariable("questionId") Long questionId,
            @PathVariable("answerId") Long answerId,
            @RequestBody AnswerDto answerDto,
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        if (!questionService.existsById(questionId)) {
            log.warn("Not found question with id: {}", questionId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!answerService.existsById(answerId)) {
            log.warn("Not found answer with id: {}", answerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (!answerDto.getId().equals(answerId)) {
            log.warn("AnswerDto id and given answer id are not equal");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if (!answerDto.getQuestionId().equals(questionId)) {
            log.warn("AnswerDto question id {} and given question id {} are not equal",
                    answerDto.getQuestionId(), questionId);
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Optional<User> recentUser = userService.getByEmail(userDetails.getUsername());
        answerDtoService.updateAnswerBody(
                recentUser.get().getId(), answerDto.getBody(), answerId, questionId);
        log.info("Updated answer with id: {}", answerId);
        return new ResponseEntity<>(answerDto, HttpStatus.OK);
    }

    @Operation(summary = "Добавление комментария к ответу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Комментарий успешно добавлен."),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе"),
            @ApiResponse(responseCode = "404", description = "Ответ с id (Id) не был найден.")
    })
    @PostMapping("/{answerId}/comment")
    public ResponseEntity<CommentAnswerDto> addCommentToAnswer(
            @PathVariable(name = "questionId") Long questionId,
            @PathVariable(name = "answerId") Long answerId,
            @AuthenticationPrincipal UserDetails userDetails,
            @Valid @RequestBody @NotNull @NotEmpty String commentText) {
        Optional<User> recentUser = userService.getByEmail(userDetails.getUsername());
        if (!answerService.existsById(answerId)) {
            log.warn("Not found Answer with id {}", answerId);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (commentText.isBlank()) {
            log.warn("Trying to add an empty comment to an answer with id {}", answerId);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        log.info("To Answer with id {} from question with id {} add comment: {}", answerId, questionId, commentText);
        return new ResponseEntity<>(
                commentAnswerDtoService.addCommentToAnswer(recentUser.get(), answerId, commentText),
                HttpStatus.CREATED);
    }

    @Operation(
            summary = "Увеличивает оценку ответа на 10")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Голос успешно зачтен, возвращаем количество голосов."),
            @ApiResponse(responseCode = "406", description = "Вы уже голосовали за данный ответ ранее."),
            @ApiResponse(responseCode = "400", description = "Автор ответа не может голосовать за него")
    })
    @PostMapping("/{answerId}/upVote")
    public ResponseEntity<?> upVoteAnswer(@PathVariable Long answerId,
                                          @AuthenticationPrincipal UserDetails userDetails) {
        Optional<User> userOpt = userService.getByEmail(userDetails.getUsername());
        Optional<Answer> answerOpt = answerService.getById(answerId);

        if (answerOpt.isEmpty() || userOpt.isEmpty()) {
            log.warn("Answer or User not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        Answer answer = answerOpt.get();
        User user = userOpt.get();

        if (answer.getUser().equals(user)) {
            log.warn("User cannot vote for their own answer");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        if (voteAnswerService.getByAnswerIdAndUserId(answerId, user.getId()).isPresent()) {
            log.warn("User has already voted for this answer");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        Long votesCountBefore = (long) answer.getVoteAnswers().size();
        voteAnswerService.upVoteAnswer(answer, user);
        Long votesCountAfter = (long) answer.getVoteAnswers().size();

        if (votesCountAfter > votesCountBefore) {
            log.info("User with ID {} upvoted Answer with ID {}", user.getId(), answerId);
        }

        return ResponseEntity.ok().body("Vote added successfully.");
    }



    @DeleteMapping("/{answerId}")
    @Operation(summary = "Помечает ответ на удаление")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешно помечено"),
            @ApiResponse(responseCode = "404", description = "Ответ не найден")
    })
    public ResponseEntity<Void> markAnswerToDelete(@PathVariable Long questionId, @PathVariable Long answerId) {

        boolean isMarked = answerService.markAnswerAsDeleted(answerId);

        log.info("Отметка ответа на вопрос с id: {} для удаления. Id ответа: {}", questionId, answerId);

        if(isMarked) {
            log.info("Ответ с ID {} успешно помечен для удаления.", answerId);
            return ResponseEntity.ok().build();
        } else {
            log.error("Ответ с ID {} не найден.", answerId);
            return ResponseEntity.notFound().build();
        }
    }

}