package com.javamentor.qa.platform.webapp.controllers.rest;

import com.javamentor.qa.platform.service.abstracts.dto.CommentDtoService;
import com.javamentor.qa.platform.service.abstracts.dto.QuestionCommentDtoService;
import com.javamentor.qa.platform.service.abstracts.model.QuestionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("api/user/question")
@AllArgsConstructor
public class QuestionController {

    private static final Logger logger = LoggerFactory.getLogger(ResourceQuestionController.class);
    private QuestionService questionService;
    /**
     * Получение количества вопросов.
     *
     * @return ResponseEntity возвращает количество вопросов и статус OK, либо NOT_FOUND, если вопросы отсутствуют в БД.
     */

    private CommentDtoService commentDtoService;


    @Autowired
    public QuestionController(CommentDtoService commentDtoService) {
        this.commentDtoService = commentDtoService;
    }

    /**
     * Получение комментариев к вопросу.
     *
     * @return ResponseEntity возвращает количество комментариев и статус HTTP запроса.
     */
    @GetMapping("/api/user/question/{id}/comment")
    @PostMapping
    @Operation(summary = "Возвращает количество комментариев к вопросу")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ответ получен успешно"),
            @ApiResponse(responseCode = "400", description = "Нет комментариев к данному вопросу"),
            @ApiResponse(responseCode = "404", description = "Ошибка в запросе")})
    public ResponseEntity<List<QuestionCommentDtoService>> getAllCommentsOnQuestionById(@PathVariable("id") Long questionId) {
        // Извлечение комментариев для заданного вопроса
        if (getAllCommentsOnQuestionById(questionId) != null) {
            List<QuestionCommentDtoService> comments = (List<QuestionCommentDtoService>) getAllCommentsOnQuestionById(questionId);
            logger.info("Возвращены комментарии для вопроса \n {}", questionId);
            return new ResponseEntity<>(comments, HttpStatus.OK);
        }
        logger.warn("Для данного вопроса не найдено комментариев");
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @GetMapping("/count")
    public ResponseEntity<Long> getCountQuestion() {
        // Получаем количество вопросов
        Long countQuestion = questionService.getCountQuestion();
        if (countQuestion != null) {
            // Успешный запрос
            logger.info("Возвращено количество вопросов: {}", countQuestion);
            return new ResponseEntity<>(countQuestion, HttpStatus.OK);
        } else {
            // Если вопросы в БД отсутствуют
            logger.warn("Количество вопросов равно null");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
