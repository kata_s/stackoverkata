package com.javamentor.qa.platform.webapp.controllers.rest;

import com.javamentor.qa.platform.models.dto.UserDto;
import com.javamentor.qa.platform.service.abstracts.dto.UserDtoService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api/user/")
@AllArgsConstructor
@Tag(name = "ResourceUserController")
public class ResourceUserController {

    private static final Logger LOGGER = Logger.getLogger(ResourceUserController.class.getName());
    private final UserDtoService userDtoService;

    @Operation(summary = "Получение пользователя по  ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Успешный запрос"),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден")
    })
    /*
     * Метод получает пользователя по его идентификатору.
     * @param id - идентификатор пользователя.
     * @return ResponseEntity с объектом UserDto в теле ответа и статусом HTTP.
     */
    @GetMapping("{id}")
    public ResponseEntity<UserDto> getUserDtoById(@PathVariable @Parameter(description = "Идентификатор пользователя") Long id) {
        LOGGER.log(Level.INFO, "Запрос на получение пользователя с идентификатором: {0}", id);

        // Получение пользователя по идентификатору
        Optional<UserDto> userDtoOptional = userDtoService.getUserById(id);

        // Проверка наличия пользователя в БД и формирование ответа
        if (userDtoOptional.isEmpty()) {
            LOGGER.log(Level.WARNING, "Пользователь с идентификатором {0} не найден", id);
            return new ResponseEntity<>(new UserDto(), HttpStatus.NOT_FOUND);
        }
        LOGGER.log(Level.INFO, "Пользователь найден: {0}", userDtoOptional.get());
        return new ResponseEntity<>(userDtoOptional.get(), HttpStatus.OK);
    }
}
