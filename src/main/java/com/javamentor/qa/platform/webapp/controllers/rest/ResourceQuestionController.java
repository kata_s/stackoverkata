package com.javamentor.qa.platform.webapp.controllers.rest;

import com.javamentor.qa.platform.mapper.QuestionMapper;
import com.javamentor.qa.platform.models.dto.QuestionCreateDto;
import com.javamentor.qa.platform.models.dto.QuestionDto;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.dto.QuestionDtoService;
import com.javamentor.qa.platform.service.abstracts.model.QuestionService;
import com.javamentor.qa.platform.service.abstracts.model.TagService;
import com.javamentor.qa.platform.service.abstracts.model.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("api/user/question")
@AllArgsConstructor
@Tag(name = "ResourceQuestionController")
public class ResourceQuestionController {
    private static final Logger logger = LoggerFactory.getLogger(ResourceQuestionController.class);

    private final UserService userService;
    private final TagService tagService;
    private final QuestionService questionService;
    private final QuestionMapper questionMapper;
    private final QuestionDtoService questionDtoService;

    @PostMapping
    @Operation(summary = "Добавляет новый вопрос, возвращает QuestionDto")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Ответ успешно добавлен"),
            @ApiResponse(responseCode = "400", description = "Ответ не добавлен, проверьте обязательные поля")})
    @ResponseBody
    public ResponseEntity<QuestionDto> addNewQuestion(@Valid @RequestBody QuestionCreateDto questionCreateDto) {
        logger.info("Получен запрос на добавление вопроса.");

        // Получение информации о текущем пользователе из Spring Security
        //TODO: исправить после реализации  Spring Security
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user;
        if (authentication != null && authentication.getPrincipal() instanceof User) {
            user = (User) authentication.getPrincipal();
            logger.info("Получен пользователь {} из Spring Security", user.getFullName());
        } else {
            user = userService.getByEmail("123@gmail.com")
                    .orElseThrow(() -> new RuntimeException("Пользователь не найден"));
            logger.warn("Используется заглушка. Spring Security не настроен");
        }

        // Валидация входных данных
        if (questionCreateDto.getTitle().isBlank()) {
            logger.info("Получен запрос с нулевым или пустым заголовком");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (questionCreateDto.getDescription().isBlank()) {
            logger.info("Получен запрос с нулевым или пустым описанием");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        if (questionCreateDto.getTags().isEmpty()) {
            logger.info("Получен запрос с нулевыми или пустыми тегами");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        // Обработка запроса
        logger.info("Запрос на создание вопроса успешно выполнен.");
        return new ResponseEntity<>(questionMapper.questionToQuestionDto(questionService.addQuestion(questionCreateDto,
                tagService.addTag(questionCreateDto), user)), HttpStatus.CREATED);
    }

    @Operation(summary = "Получение вопроса по ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Вопрос получен"),
            @ApiResponse(responseCode = "400", description = "Ошибка при поиске вопроса"),
            @ApiResponse(responseCode = "404", description = "Вопрос не найден")

    })
    @GetMapping("/{id}")
    public ResponseEntity<QuestionDto> getQuestionById(@PathVariable Long id, @AuthenticationPrincipal UserDetails userDetails) {
        Optional<User> user = userService.getByEmail(userDetails.getUsername());
        Long userId = user.get().getId();
        logger.info("Получен id авторизированного пользователя");

        Optional<QuestionDto> questionDtoOptional = questionDtoService.getQuestionById(id, userId);
        if (questionDtoOptional.isEmpty()) {
            logger.warn("Вопрос с данным id не найден");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        logger.info("Вопрос получен");
        return new ResponseEntity<>(questionDtoOptional.get(), HttpStatus.OK);
    }
}

