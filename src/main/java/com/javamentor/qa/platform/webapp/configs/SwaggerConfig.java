package com.javamentor.qa.platform.webapp.configs;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
@OpenAPIDefinition(
        info = @Info(
                title = "StackOverKata System ",
                description = "StackOverKata", version = "1.0.0",
                contact = @Contact(
                        name = "KataEducation",
                        email = "info@kata.academy",
                        url = "https://kata.academy//"
                )
        )
)
@SecurityScheme(
        name = "JWT",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)
public class SwaggerConfig {

}
