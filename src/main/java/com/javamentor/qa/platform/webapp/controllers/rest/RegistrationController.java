package com.javamentor.qa.platform.webapp.controllers.rest;

import com.javamentor.qa.platform.models.dto.UserRegistrationDto;
import com.javamentor.qa.platform.models.entity.user.User;
import com.javamentor.qa.platform.service.abstracts.model.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user/registration")
@AllArgsConstructor
@NoArgsConstructor
@Slf4j
@Tag(name = "RegistrationController")
public class RegistrationController {
    private UserService userService;

    @Value("${EXPIRATION_TIME_IN_MINUTES}")
    private int EXPIRATION_TIME_IN_MINUTES;
    @Value("${spring.mail.username}")
    private String fromAddress;
    @Value("${spring.mail.username}")
    private String senderName;
    @Value("${spring.mail.host}")
    private String host;

    @Autowired
    public RegistrationController(UserService userService) {
        this.userService = userService;
    }

    @Operation(summary = "Отправка сообщения для подтверждения почты")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Сообщение успешно отправлено"),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе")})
    @PostMapping
    public ResponseEntity<UserRegistrationDto> sendMessage(@RequestBody UserRegistrationDto userRegistrationDto) {
        if (!userRegistrationDto.getEmail().isEmpty()) {
            userService.registration(userRegistrationDto);
            log.info("Message sent");
            return ResponseEntity.ok(userRegistrationDto);
        }
        log.warn("Given email is empty");
        return ResponseEntity.badRequest().build();
    }

    @Operation(summary = "Верификация и регистрация пользователя")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Пользователь успешно зарегестрирован"),
            @ApiResponse(responseCode = "400", description = "Ошибка в запросе"),
            @ApiResponse(responseCode = "404", description = "Пользователь не найден")})
    @GetMapping("/verify/{code}")
    public ResponseEntity<String> verify(@PathVariable Integer code, @RequestBody String email) {
        if (userService.getByEmail(email).isEmpty()) {
            log.warn("Given email does not exist");
            return ResponseEntity.badRequest().build();
        }
        User user = userService.getByEmail(email).get();
        if (user.getPersistDateTime().isBefore(user.getPersistDateTime().plusMinutes(EXPIRATION_TIME_IN_MINUTES))) {
            if (userService.verification(user, code)) {
                log.info("Successfully verificated user");
                return ResponseEntity.ok(email);
            }
            log.warn("Given verification code and sent code are not equal");
            return ResponseEntity.badRequest().build();
        }
        log.warn("Verification code has expired");
        return ResponseEntity.badRequest().build();
    }
}
