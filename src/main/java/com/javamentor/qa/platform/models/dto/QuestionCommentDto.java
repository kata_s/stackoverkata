package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class QuestionCommentDto {
    @Schema(description = "ID комментария")
    private Long id;
    @Schema(description = "ID вопроса")
    private Long questionId;
    @Schema(description = "Дата редактирования вопроса")
    private LocalDateTime lastRedactionDate;

    @Schema(description = "Дата создания вопроса")
    private LocalDateTime persistDate;
    @NotNull
    @NotEmpty
    @Schema(description = "Текст вопроса")
    private String text;

    @NotNull
    @Schema(description = "ID пользователя")
    private Long userId;

    @Schema(description = "Картинка пользователя")
    private String imageLink;

    @Schema(description = "Репутация")
    private Long reputation;
}
