package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Регистрация пользователя")
public class UserRegistrationDto implements Serializable {
    @NotEmpty
    @Schema(description = "Имя пользователя")
    private String firstName;
    @NotEmpty
    @Schema(description = "Фамилия пользователя")
    private String lastName;
    @NotEmpty
    @Schema(description = "Почта пользователя")
    private String email;
    @NotEmpty
    @Schema(description = "Пароль пользователя")
    private String password;
}
