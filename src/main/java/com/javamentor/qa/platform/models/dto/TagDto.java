package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
@Setter
@Schema(description = "Тэг")
public class TagDto implements Serializable {
    @Parameter(description = "id тэга")
    private Long id;
    @Schema(description = "название тэга")
    private String name;
    @Schema(description = "описание тэга")
    private String description;

    public TagDto() {
    }

    public TagDto(String name) {
        this.name = name;
    }

}
