package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Schema(description = "запрос аутентификации")
public class AuthenticationRequestDto {
    @NotBlank
    @Schema(description = "логин")
    private String login;
    @NotBlank
    @Schema(description = "пароль")
    private String pass;
}
