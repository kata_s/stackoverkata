package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Schema(description = "DTO для представления списка объектов с постраничной навигацией.")
public class PageDto<T> {
    @Schema(description = "Номер текущей страницы.")
    private int currentPageNumber;
    @Schema(description = "Общее количество страниц.")
    private int totalPageCount;
    @Schema(description = "Общее количество результатов.")
    private int totalResultCount;
    @Schema(description = "Список элементов на текущей странице.")
    private List<T> items;
    @Schema(description = "Количество элементов на странице.")
    private int itemsOnPage;
}
