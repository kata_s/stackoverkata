package com.javamentor.qa.platform.models.dto;

import com.javamentor.qa.platform.models.entity.user.User;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DTO for {@link User}
 */
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Schema(description = "пользователь")
public class UserDto implements Serializable {
    @Parameter(description = "id пользователя")
    private Long id;
    @Schema(description = "почта пользователя")
    private String email;
    @Schema(description = "имя пользователя")
    private String fullName;
    @Schema(description = "ссылка на изображение пользователя")
    private String linkImage;
    @Schema(description = "город пользователя")
    private String city;
    @Schema(description = "репутация пользователя")
    private Long reputation;
    @Schema(description = "дата регистрации пользователя")
    private LocalDateTime registrationDate;
    @Schema(description = "количество голосов пользователя")
    private Long votes;

}
