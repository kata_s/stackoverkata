package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Schema(description = "JWT токен")
public class TokenResponseDto {
    @NotEmpty
    @Schema(description = "роль")
    private String role;
    @NotEmpty
    @Schema(description = "токен")
    private String token;
}
