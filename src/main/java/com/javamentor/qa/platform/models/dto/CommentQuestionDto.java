package com.javamentor.qa.platform.models.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Schema(description = "Комментарии вопроса")
public class CommentQuestionDto {

    @Schema(description = "ID комментария")
    private Long id;

    @Schema(description = "ID вопроса")
    private Long questionId;

    @Schema(description = "дата последнего редактирования")
    private LocalDateTime lastRedactionDate;

    @Schema(description = "дата создания комментария")
    private LocalDateTime persistDate;

    @NotNull
    @NotEmpty
    @Schema(description = "текст комментария")
    private String text;

    @NotNull
    @Schema(description = "ID пользователя")
    private Long userId;

    @Schema(description = "ссылка на картинку пользователя")
    private String imageLink;

    @Schema(description = "репутация пользователя")
    private Long reputation;

}