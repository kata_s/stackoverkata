package com.javamentor.qa.platform.exception;

public class AnswerException extends RuntimeException {
    public AnswerException() {
    }

    public AnswerException(String message) {
        super(message);
    }

    public AnswerException(String s, Long answerId) {
    }
}
