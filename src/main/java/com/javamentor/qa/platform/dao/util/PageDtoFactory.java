package com.javamentor.qa.platform.dao.util;

import com.javamentor.qa.platform.models.dto.PageDto;

import java.util.List;

public class PageDtoFactory {
    public static <T> PageDto <T> createPageDto(int currentPageNumber, int totalPageCount, int totalResultCount, List<T> items,int itemsOnPage){
        PageDto<T> pageDto = new PageDto<>();
        pageDto.setCurrentPageNumber(currentPageNumber);
        pageDto.setTotalPageCount(totalPageCount);
        pageDto.setTotalResultCount(totalResultCount);
        pageDto.setItems(items);
        pageDto.setItemsOnPage(itemsOnPage);
        return pageDto;
    }
}
