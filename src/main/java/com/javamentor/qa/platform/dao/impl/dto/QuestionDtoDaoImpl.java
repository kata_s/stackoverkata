package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.QuestionDtoDao;
import com.javamentor.qa.platform.models.dto.QuestionDto;
import com.javamentor.qa.platform.models.dto.TagDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Repository
public class QuestionDtoDaoImpl implements QuestionDtoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<QuestionDto> getById(Long questionId, Long authorizedUserId) {
        TypedQuery<QuestionDto> query = entityManager
                .createQuery(
                        """
                                SELECT
                                new com.javamentor.qa.platform.models.dto.QuestionDto (
                                q.id,
                                q.title,
                                u.id,
                                u.fullName,
                                u.imageLink,
                                q.description,
                                                                
                                (SELECT  count(qv.id) FROM QuestionViewed qv WHERE qv.question.id = q.id),
                                                                
                                COALESCE ((SELECT SUM(r.count) FROM Reputation r WHERE r.author.id = u.id),0),
                                                                
                                (SELECT count(a.id) FROM Answer a WHERE a.question.id = q.id),
                                                                
                                (SELECT SUM(CASE WHEN (v.vote = 'UP') THEN 1 
                                WHEN (v.vote = 'DOWN') THEN -1 ELSE 0 END)
                                FROM VoteQuestion v WHERE v.question.id = q.id),
                                                                
                                q.persistDateTime,
                                q.lastUpdateDateTime,
                                                                
                                (SELECT count(vq.vote) FROM VoteQuestion vq WHERE vq.question.id = q.id),   
                                            
                                (SELECT vq.vote FROM VoteQuestion vq WHERE vq.question.id = q.id 
                                AND vq.user.id =:userId)             
                                                                                               
                                )                                                            
                                                                                                                                                                                                
                                                           
                                FROM Question q  JOIN q.user u  WHERE q.id = :questionId                                                 
                                """, QuestionDto.class);
        query.setParameter("questionId", questionId);
        query.setParameter("userId", authorizedUserId);

        TypedQuery<TagDto> queryTag = entityManager
                .createQuery("SELECT new com.javamentor.qa.platform.models.dto.TagDto" +
                        "(t.id," +
                        "t.name," +
                        "t.description)" +
                        "from Tag t join t.questions qt where qt.id=:questionId", TagDto.class);
        queryTag.setParameter("questionId", questionId);
        List<TagDto> tagList = queryTag.getResultList();
        Optional<QuestionDto> questionDto = Optional.ofNullable(query.getSingleResult());
        QuestionDto questionDto1 = questionDto.get();
        questionDto1.setListTagDto(tagList);

        return Optional.of(questionDto1);

    }
}
