package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.UserDtoDao;
import com.javamentor.qa.platform.dao.util.SingleResultUtil;
import com.javamentor.qa.platform.models.dto.UserDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Repository
public class UserDtoDaoImpl implements UserDtoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<UserDto> getUserById(Long id) {
        TypedQuery<UserDto> query = entityManager.createQuery(
                """
                        SELECT
                            new com.javamentor.qa.platform.models.dto.UserDto(
                                u.id, u.email, u.fullName, u.imageLink, u.city,
                                (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r WHERE r.author.id = :userId),
                                u.persistDateTime,
                                (SELECT COALESCE(count(a.voteType), 0) FROM VoteAnswer a WHERE a.user.id = :userId) +
                                (SELECT COALESCE(count(q.vote), 0) FROM VoteQuestion q WHERE q.user.id = :userId)
                            )
                        FROM User u WHERE u.id = :userId
                        """,
                UserDto.class
        );
        query.setParameter("userId", id);
        return SingleResultUtil.getSingleResultOrNull(query);
    }
}