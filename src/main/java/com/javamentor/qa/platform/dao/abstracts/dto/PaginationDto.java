package com.javamentor.qa.platform.dao.abstracts.dto;

import org.springframework.data.domain.Page;

public interface PaginationDto<T> extends Page<T> {
}
