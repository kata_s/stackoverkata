package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.CommentAnswerDtoDao;
import com.javamentor.qa.platform.models.dto.CommentAnswerDto;
import com.javamentor.qa.platform.models.entity.question.answer.CommentAnswer;
import com.javamentor.qa.platform.models.entity.user.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class CommentAnswerDtoDaoImpl implements CommentAnswerDtoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public CommentAnswerDto getCommentAnswerDto(CommentAnswer commentAnswer, User user) {
        return entityManager.createQuery("""
                        select new com.javamentor.qa.platform.models.dto.CommentAnswerDto(
                        c.id,
                        a.id, a.updateDateTime, a.persistDateTime,
                        c.text,
                        u.id, u.imageLink,
                        (select coalesce(sum(r.count), 0) from Reputation r where r.author.id = :userId))
                        from Answer a
                        join User u on u.id = :userId
                        left join Comment c on c.id = :commentId
                        where a.id = :answerId
                        """, CommentAnswerDto.class).setParameter("commentId", commentAnswer.getId())
                .setParameter("answerId", commentAnswer.getAnswer().getId())
                .setParameter("userId", user.getId())
                .getSingleResult();
    }
}
