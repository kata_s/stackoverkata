package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.PageDtoDao;
import com.javamentor.qa.platform.dao.abstracts.dto.PaginationDto;
import com.javamentor.qa.platform.models.dto.PageDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public abstract class PageDtoDaoImpl<T, P> implements PageDtoDao<T, P> {
    @PersistenceContext
    private EntityManager entityManager;

    private final Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
            .getActualTypeArguments()[0];
    private final PageDto<T> pageDto;
    private final Map<String, PaginationDto<T>> paginationDaoMap;

    public PageDtoDaoImpl(Map<String, PaginationDto<T>> pageMap) {
        this.paginationDaoMap = pageMap;
        pageDto = build();
    }

    public PageDto<T> getPageDto(Map<String, Object> parameters) {

        String workPagination = (String) parameters.get("workPagination");
        PaginationDto<T> pagination = paginationDaoMap.get(workPagination);

        pageDto.setCurrentPageNumber(pagination.getNumber());
        pageDto.setTotalPageCount(pagination.getTotalPages());
        pageDto.setItemsOnPage((int)pagination.getTotalElements());
        return pageDto;
    }
    @Override
    public List<T> getItems(P param) {

        List<T> itemList = entityManager
                .createQuery("FROM " + entityClass.getName() + " e WHERE e.questionId =: param")
                .setParameter("param", param)
                .setFirstResult((pageDto.getCurrentPageNumber() - 1) * pageDto.getTotalPageCount())
                .setMaxResults(pageDto.getTotalPageCount())
                .getResultList();

        pageDto.setItems(itemList);
        return itemList;
    }

    @Override
    public int getTotalResultCount(P param) {

        int count = (Integer) entityManager
                .createQuery("SELECT COUNT(*) FROM " + entityClass.getName() + " e WHERE e.id =: param")
                .setParameter("param", param)
                .getSingleResult();

        pageDto.setTotalResultCount(count);
        return count;
    }

    public abstract PageDto<T> build();
}
