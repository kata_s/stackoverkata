package com.javamentor.qa.platform.dao.abstracts.dto;

import com.javamentor.qa.platform.models.dto.PageDto;

import java.util.List;

public interface PageDtoDao<T, P> {
    List<T> getItems(P param);
    int getTotalResultCount(P param);
}
