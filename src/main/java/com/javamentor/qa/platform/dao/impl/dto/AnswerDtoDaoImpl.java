package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.AnswerDtoDao;
import com.javamentor.qa.platform.models.dto.AnswerDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class AnswerDtoDaoImpl implements AnswerDtoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<AnswerDto> getAllAnswersDtoByQuestionId(Long questionId, Long userId) {
        return entityManager.createQuery("""
                                SELECT 
                                    new com.javamentor.qa.platform.models.dto.AnswerDto(
                                        a.id,
                                        a.user.id,
                                        a.question.id,
                                        a.htmlBody, 
                                        a.persistDateTime, 
                                        a.isHelpful, 
                                        a.dateAcceptTime,
                              
                                        (SELECT SUM(CASE WHEN (v.voteType = 'UP') THEN 1
                                        WHEN (v.voteType = 'DOWN') THEN -1 ELSE 0 END)
                                        FROM VoteAnswer v WHERE v.answer.id = a.id),
                                        
                                        (SELECT COALESCE(SUM(r.count), 0) FROM Reputation r
                                        WHERE r.author.id = a.user.id),
                                        
                                        a.user.imageLink,
                                        a.user.fullName, 
                                        
                                        (SELECT COUNT(v.id) FROM VoteAnswer v WHERE v.answer.id = a.id) AS countVote,
                                        
                                        (SELECT COALESCE(v.voteType, 'null') FROM VoteAnswer v
                                        WHERE v.user.id = :userId and v.answer.id = a.id) AS voteType
                                        )
                                FROM Answer a WHERE a.question.id= :questionId AND a.user.id=:userId
                                               """,
                        AnswerDto.class)
                .setParameter("questionId", questionId)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void updateAnswerBody(Long userId, String body, Long answerId, Long questionId) {
        entityManager.createQuery("""
                        UPDATE
                        Answer a
                        SET a.htmlBody= :body
                        WHERE a.user.id= :userId
                        AND a.id= :answerId
                        AND a.question.id = :questionId
                        """)
                .setParameter("userId", userId)
                .setParameter("body", body)
                .setParameter("answerId", answerId)
                .setParameter("questionId", questionId)
                .executeUpdate();
    }
}
