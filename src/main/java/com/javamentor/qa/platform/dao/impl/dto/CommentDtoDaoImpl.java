package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.CommentDtoDao;
import com.javamentor.qa.platform.models.dto.CommentQuestionDto;
import com.javamentor.qa.platform.models.dto.QuestionCommentDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository
public class CommentDtoDaoImpl implements CommentDtoDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CommentQuestionDto> getAllQuestionCommentDtoById(Long questionId) {
        return entityManager.createQuery(
                        "SELECT new com.javamentor.qa.platform.models.dto.QuestionCommentDto(cq.questionId, q.persistDateTime, q.id, q.title) " +
                        "FROM Question q " +
                        "JOIN CommentQuestion cq " +
                        "WHERE cq.question.commentQuestions = :questionId", CommentQuestionDto.class)
                .setParameter("questionId", questionId)
                .getResultList();
    }
}
