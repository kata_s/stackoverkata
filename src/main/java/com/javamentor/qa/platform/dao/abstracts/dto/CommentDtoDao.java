package com.javamentor.qa.platform.dao.abstracts.dto;

import com.javamentor.qa.platform.models.dto.CommentQuestionDto;

import java.util.List;

public interface CommentDtoDao {
    List<CommentQuestionDto> getAllQuestionCommentDtoById(Long questionId);
}
