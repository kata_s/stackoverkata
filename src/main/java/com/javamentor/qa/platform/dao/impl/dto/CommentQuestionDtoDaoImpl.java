package com.javamentor.qa.platform.dao.impl.dto;

import com.javamentor.qa.platform.dao.abstracts.dto.CommentDtoDao;
import com.javamentor.qa.platform.models.dto.CommentQuestionDto;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class CommentQuestionDtoDaoImpl implements CommentDtoDao {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<CommentQuestionDto> getAllQuestionCommentDtoById(Long questionId) {
        return entityManager.createQuery("""
                            SELECT NEW com.javamentor.qa.platform.models.dto.CommentQuestionDto(
                                c.id AS commentId,
                                c.text,
                                q.id AS questionId,
                                q.lastRedactionDate,
                                q.persistDate,
                                u.id AS userId,
                                u.imageLink,
                                COALESCE((SELECT SUM(r.count) FROM Reputation r WHERE r.author.id = u.id), 0) AS reputation
                            )
                            FROM Comment c
                            JOIN c.question q
                            JOIN c.user u
                            WHERE c.id = :questionId
                        """, CommentQuestionDto.class)
                .setParameter("questionId", questionId)
                .getResultList();
    }
}
