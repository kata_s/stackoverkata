package com.javamentor.qa.platform.converters;

import com.javamentor.qa.platform.models.dto.UserRegistrationDto;
import com.javamentor.qa.platform.models.entity.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public abstract class UserConverter {
    @Mapping(source = "email", target = "email")
    @Mapping(source = "password", target = "password")
    @Mapping(target = "fullName", expression = "java(userRegistrationDto.getFirstName() + \" \" + userRegistrationDto.getLastName())")
    public abstract User userRegistrationDtoToUser(UserRegistrationDto userRegistrationDto);

    @Mapping(source = "email", target = "email")
    @Mapping(source = "password", target = "password")
    @Mapping(source = "fullName", target = "firstName", qualifiedByName = "userFullNameToFirstName")
    @Mapping(source = "fullName", target = "lastName", qualifiedByName = "userFullNameToLastName")
    public abstract UserRegistrationDto userToUserRegistrationDto(User user);

    @Named("userFullNameToFirstName")
    public String userFullNameToFirstName(String fullName) {
        return fullName.split(" ")[0];
    }

    @Named("userFullNameToLastName")
    public String userFullNameToLastName(String fullName) {
        return fullName.split(" ")[1];
    }

}
