let openListId = null;

document.addEventListener('show.bs.collapse', function (event) {
    const listId = event.target.id;

    // Закрыть текущий открытый список, если таковой есть
    if (openListId && openListId !== listId) {
        const openList = document.getElementById(openListId);
        openList.classList.remove('show');
    }

    // Обновить текущий открытый список
    openListId = listId;
});

document.addEventListener('hide.bs.collapse', function (event) {
    const listId = event.target.id;

    // Если текущий открытый список совпадает с закрываемым, обновить
    if (openListId === listId) {
        openListId = null;
    }
});

document.querySelector('.sidebar').innerHTML = `
<aside class="bg-white p-2 sidebar">
                    <div class="colorHeaderSidebar">
                        Шаг 1: Создайте черновик своего вопроса
                        <br />
                        <hr />
                    </div>
                    <div>
                        <pre>
                            Сообщество здесь, чтобы помочь вам с конкретными проблемами по программированию,
                            алгоритмам, языкам программирования.
                        </pre>
                        <pre>
                            Избегайте публикации вопросов, на которые невозможно дать объективный ответ.
                        </pre>
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <b>
                            <p style="color: black" class="dropdown-toggle text" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample"
                               aria-expanded="false" aria-controls="collapseExample">
                                1. Опишите проблему кратко
                            </p>
                        </b>
                    </div>
                    <hr />
                    <div class="collapse" id="collapseExample">
                        <ul>
                            <li>
                                <pre>Включите подробную информацию о вашей цели</pre>
                            </li>
                            <li>
                                <pre>Опишите, что вы ожидали и что получили</pre>
                            </li>
                            <li>
                                <pre>Добавьте сообщения об ошибках</pre>
                            </li>
                        </ul>
                    </div>

                    <div class="d-flex justify-content-between align-items-center">
                        <b>
                            <p style="color: black" class="dropdown-toggle text" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample2"
                               aria-expanded="false" aria-controls="collapseExample2">
                                2. Опишите, что вы пробовали
                            </p>
                        </b>
                    </div>
                    <hr />
                    <div class="collapse content" id="collapseExample2">
                       <pre>
                            Покажите, что вы пробовали,
                            что вы нашли (на этом сайте или в другом месте),
                            и почему это не решает вашу проблему. Вы можете
                            получить лучшие ответы, когда продемонстрируете
                            ваше исследование.
                       </pre>
                    </div>

                    <div class="d-flex justify-content-between align-items-center">
                        <b>
                            <p style="color: black" class="dropdown-toggle text" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample3"
                               aria-expanded="false" aria-controls="collapseExample3">
                                3. Добавьте немного кода
                            </p>
                        </b>
                    </div>
                    <div class="collapse content" id="collapseExample3">
                       <pre>
                            По возможности добавьте минимальный код,
                           необходимый для воспроизведения вашей проблемы
                           (так называемый <a class="hrefex" href="#">минимальный воспроизводимый пример</a>)
                       </pre>
                    </div>
                </aside>
                <br />
                <br />
                <div class="bg-white p-3 side2">
                    <div class="d-flex justify-content-between align-items-center">
                        <b>
                            <a style="color: black" class="dropdown-toggle text btn-bg" type="button" data-bs-toggle="collapse" data-bs-target="#collapseExample4"
                               aria-expanded="false" aria-controls="collapseExample4">
                                Больше полезных ссылок
                            </a>
                        </b>
                    </div>
                    <div class="collapse content" id="collapseExample4" >
                       <pre>
                           Подробнее о том, <a style="color: dodgerblue" href="#">как задать качественный вопрос</a>.
                       </pre>
                        <pre>
                           Посетите <a style="color: dodgerblue" href="#">справочный центр.</a>
                       </pre>
                        <pre>
                           Вопросы о правилах и работе сайта можно задать на
                           <a style="color: dodgerblue" href="#">Мете.</a>
                       </pre>
                    </div>
                </div>
            </div>
`;

