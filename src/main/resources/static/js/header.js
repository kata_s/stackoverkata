function toggleMenu() {
    const menu = document.getElementById("dropdownMenu");
    const button = document.getElementById("dropdownButton");

    if (menu.style.display === "none" || menu.style.display === "") {
        menu.style.display = "block";
        button.src = "/images/img/toggle2.png";
    } else {
        menu.style.display = "none";
        button.src = "/images/img/toggle1.png";
    }
}


document.querySelector('header').innerHTML = `
<nav class="navbar navbar-expand-lg navbar-light bg-light">
   <div class="container-md d-flex justify-content-between align-items-center">
            <div class="dropdown">
                <img id="dropdownButton" src="/images/img/toggle1.png" alt="Dropdown" onclick="toggleMenu()">
                <div id="dropdownMenu" class="dropdown-content">
                    <br />
                    <a href="#">
                        <img src="/images/img/home.png" width="29" height="24" alt=""/>
                        Главная</a>
                    <a href="#">
                        <img src="/images/img/question.png" width="29" height="24" alt=""/>
                        Вопросы
                    </a>
                    <a href="#">
                        <img src="/images/img/tags.png" width="29" height="28" alt=""/>
                        Метки
                    </a>
                    <a href="#"></a>
                    <a href="#">
                        <img src="/images/img/members.png" width="29" height="23" alt=""/>
                        Участники
                    </a>
                    <a href="#">
                        <img src="/images/img/unreply.png" width="29" height="26" alt=""/>
                        Неотвеченные
                    </a>
                </div>
            </div>
            <a class="navbar-brand" href="#">
                <img src="/images/img/logo.png" alt="Bootstrap" width="166" height="39">
            </a>
            <form class="d-flex flex-grow-1 me-3">
                <input class="form-control me-2 flex-grow-1 resized w-100 icon nosubmit" type="search" placeholder="Поиск" aria-label="Поиск">
                <button class="btn btn-primary btn-sm another-style" type="submit">Присоединиться к сообществу</button>
            </form>
        </div>
</nav>
`;
