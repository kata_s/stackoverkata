
document.querySelector('footer').innerHTML = `
<div style="margin-left: 280px">
        <div class="row">
            <div class="col" style="text-align: left;">
                <a href="#">STACK OVERFLOW НА РУССКОМ</a>
                <br/>
                <br/>
                <a href="#">Тур</a>
                <br />
                <a href="#">Справка</a>
                <br />
                <a href="#">Чат</a>
                <br />
                <a href="#">Связаться с нами</a>
                <br />
                <a href="#">Оставить отзыв</a>
            </div>
            <div class="col" style="text-align: left;">
                <a href="#">КОМПАНИЯ</a>
                <br/>
                <br/>
                <a href="#">Stack Overflow</a>
                <br />
                <a href="#">Teams</a>
                <br />
                <a href="#">Advertising</a>
                <br />
                <a href="#">Collectives</a>
                <br />
                <a href="#">Talent</a>
                <br />
                <a href="#">О компании</a>
                <br />
                <a href="#">Упоминания в СМИ</a>
                <br />
                <a href="#">Соглашение</a>
                <br />
                <a href="#">Политика конфиденциальности</a>
                <br />
                <a href="#">Условия использования</a>
                <br />
                <a href="#">Настройки файлов cookie</a>
                <br />
                <a href="#">Политика о Cookie</a>
            </div>
            <div class="col" style="text-align: left;">
                <a href="#">СЕТЬ STACK EXCHANGE</a>
                <br/>
                <br/>
                <a href="#">Технологии</a>
                <br />
                <a href="#">Культура и отдых</a>
                <br />
                <a href="#">Жизнь и искусство</a>
                <br />
                <a href="#">Наука</a>
                <br />
                <a href="#">Специализация</a>
                <br />
                <a href="#">Бизнес</a>
                <br />
                <a href="#">API</a>
                <br />
                <a href="#">Данные</a>
            </div>
            <div class="col" style="text-align: left;">
                <a href="#">Блог</a>
                <span class="spacer"></span>
                <a href="#">Facebook</a>
                <span class="spacer"></span>
                <a href="#">Твиттер</a>
                <span class="spacer"></span>
                <a href="#">LinkedIn</a>
                <span class="spacer"></span>
                <a href="#">Instagram</a>
                <p style="margin-top: 230px;">
                    <small>
                        Дизайн сайта / логотип © 2024 Stack Exchange Inc;
                        пользовательские материалы лицензированы в соответствии с CC BY-SA. rev 2024.1.10.3270
                    </small>
                </p>
            </div>
        </div>
    </div>
`;
